var express = require('express');
var path = require('path');
var app = express();
var server = require('http').Server(app);
var socketIo = require('socket.io')(server);
/*
ASSUMPTION : data is coming from socket server in format:

  data = [
    {price:'100',vendor:'vendor1',item : 'item1'},
    {price:'200',vendor:'vendor2',item : 'item2'},
    {price:'300',vendor:'vendor3',item : 'item2'},
    {price:'400',vendor:'vendor4',item : 'item1'},
    {price:'100',vendor:'vendor3',item : 'item2'},
    {price:'400',vendor:'vendor5',item : 'item4'},
    {price:'300',vendor:'vendor1',item : 'item1'}
  ]
  in this format.

  and data array is JSON stringified.
*/


var 
    // setting the port.
    port = process.env.PORT || 8080,

    // set the socket server url from here.its default for testing the app.
    // testing socket server is implemented in cron.js file.

    SOCKET_SERVER_URL = 'http://localhost:9000';

var ioclient = require('socket.io-client')(SOCKET_SERVER_URL);
var SocketFile = require('./socketIO')(socketIo,ioclient);


app.get('/',function(req,res){
		res.render('index');
});


app.use('/assets',express.static(path.join(__dirname,'public')));
app.use('/plugins',express.static(path.join(__dirname,'node_modules')));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);

  res.render('error',{msg : err.message});
});

server.listen(port);
module.exports = app;
