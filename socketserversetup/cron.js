/* TEST SETUP FOR SOCKET IO SERVER WHICH DELIEVERS THE DATA. */

var socketIO = require('socket.io');
var http = require('http');
var app = http.createServer(function(req,res){
		res.end('index page');
});


var io = socketIO(app);
io.on('connection', function(socket) {
	console.log('Got a connection from nodejs server with socketid : ' + socket.id);
});

// sample data to give to the nodejs server.
var dataarray = [
	{price:'100',vendor:'vendor1',item : 'item1'},
	{price:'200',vendor:'vendor2',item : 'item2'},
	{price:'300',vendor:'vendor3',item : 'item2'},
	{price:'400',vendor:'vendor4',item : 'item1'},
	{price:'100',vendor:'vendor3',item : 'item2'},
	{price:'400',vendor:'vendor5',item : 'item4'},
	{price:'300',vendor:'vendor1',item : 'item1'}
];


var 
  //setting the CHUNK_SIZE i.e. number of objects to send in a chunk.
  CHUNK_SIZE = 2,
  // start point of chunk in the array.
  start=0,
  // temprorary variables
  data,p;


// this listener fires every 5 sec and fetches CHUNK_SIZE number of objects and keep doing it in cycle.
var interval = setInterval(function(){
  		console.log('emitting datachunk event');
  		if(start + CHUNK_SIZE > dataarray.length - 1){
  				data = dataarray.slice(start,dataarray.length);				
  				p = dataarray.slice(0,CHUNK_SIZE - (dataarray.length- start)); 			
  				data = data.concat(p);
  				start = CHUNK_SIZE - (dataarray.length- start);				
  		}
  		else{
  			data = dataarray.slice(start,start+CHUNK_SIZE);
  			start = start + CHUNK_SIZE; 
  		}
  		
  		io.sockets.emit('data',data);
  		console.log(JSON.stringify(data));
  				
},5000);
app.listen(9000);