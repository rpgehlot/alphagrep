const stream = require('stream');
const util = require('util');

function ReadableStream(options){
		if(! this instanceof ReadableStream)
				return new ReadableStream(options);

		stream.Readable.call(this,options);
		this.Chunkdata = [];
}

util.inherits(ReadableStream,stream.Readable);

ReadableStream.prototype.setChunk = function(chunk){
		console.log('setting chunk');
		chunk = JSON.parse(chunk);
		if(util.isArray(chunk)){
			this.Chunkdata = this.Chunkdata.concat(chunk);
		}
		else this.Chunkdata = this.Chunkdata.concat([].push(chunk));
		console.log(this.Chunkdata);
}

ReadableStream.prototype._read = function(size){
		console.log('inside _read method');
		console.log(this.Chunkdata);
		if(this.Chunkdata.length===0)
			this.push(null);
		else{
			for(var i = 0 ; i < this.Chunkdata.length ;i++){
					console.log("i = " + i);
					this.push(this.Chunkdata[i]);
			}		
		}
};

module.exports = ReadableStream;