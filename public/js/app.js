(function(socket,angular){

	"use strict";

	var MainApp = angular.module('MainApp',['ngMaterial']);
	
	MainApp.service('backendservice',['$http','$timeout',function($http,$timeout){
		this.emitEvent = function(event){
				socket.emit(event);
				return;
		};

		this.registerListener  = function(event,listener){
				socket.on(event,listener);
				return;
		};		
	}]);


	MainApp.controller('MainController',['$scope','backendservice',function($scope,backendservice){
			
			$scope.items = [];
			var temp=[],hashMap = {},uniqueVendors={};
			backendservice.emitEvent('getPreviousData');

			backendservice.registerListener('previousData',function(data){
		  		console.log('previousData');
		  		data  = JSON.parse(data);
		  		console.log(data);
		  		var item,tmp,change;
		  		for( var i = 0 ; i < data.length ; i++ ){
		  				item = data[i];
		  				item['change'] = 0;
		  				if(!uniqueVendors.hasOwnProperty(item.vendor))
		  						uniqueVendors[item.vendor] = true;

		  				if(hashMap.hasOwnProperty(item.item)){
		  						tmp = hashMap[item.item];
		  						if(tmp.hasOwnProperty(item.vendor)){
		  								
		  								tmp[item.vendor]['price'] = item.price;
		  								
		  								change = item.price - temp[tmp[item.vendor]['pos']]['price'];
		  								temp[tmp[item.vendor]['pos']]['price'] = item.price;
		  								temp[tmp[item.vendor]['pos']]['change'] = change;
		  								// console.log(hashMap);
		  						}
		  						else{
		  							tmp[item.vendor] = {};
		  							tmp[item.vendor]['price'] = item.price;
		  							tmp[item.vendor]['pos'] = temp.length;
		  							temp.push(item);
		  							// console.log(hashMap);
		  						}
		  				}
		  				else{
		  					hashMap[item.item] = {};
		  					hashMap[item.item][item.vendor]={
		  									'price' : item.price,
		  									'pos' : temp.length
		  							};		  							
		  					 
		  					temp.push(item);
		  					// console.log(hashMap);
		  				}
		  		}
		  		console.log(hashMap);
		  		$scope.$apply(function(){
		  			$scope.items = temp;
		  		});
		  	});

		  	backendservice.registerListener('_data_',function(data){
		  		console.log('realtime data');
		  		console.log(data);
		  		data = JSON.parse(data);
		  		var item,tmp,change;
		  		$scope.$apply(function(){
		 	 		for(var i = 0 ; i < data.length ; i++){
		 	 			item = data[i];
		 	 			if(!uniqueVendors.hasOwnProperty(item.vendor))
		  						uniqueVendors[item.vendor] = true;
		  				if(hashMap.hasOwnProperty(item.item)){
		  						tmp = hashMap[item.item];
		  						if(tmp.hasOwnProperty(item.vendor)){
		  							tmp[item.vendor]['price'] = item.price;
		  							change = item.price - temp[tmp[item.vendor]['pos']]['price'];
		  							temp[tmp[item.vendor]['pos']]['price'] = item.price;
		  							temp[tmp[item.vendor]['pos']]['change'] = change;
		  						}
		  						else{
		  							tmp[item.vendor] = {};
		  							tmp[item.vendor]['price'] = item.price;
		  							tmp[item.vendor]['pos'] = $scope.items.length;
		  							temp.push(item);
		  						}
		  				}
		  				else{

		  					hashMap[item.item] = {};
		  					hashMap[item.item][item.vendor]={
		  									'price' : item.price,
		  									'pos' : $scope.items.length
		  							};		 
		  					temp.push(data[i]);
		  				}
		  			}
		  			if($scope.isFilterSelected === false)
		  				$scope.items = temp;
		  		});
			});




			/*-----------------------------------*/
			$scope.selectedFilter = '';
			$scope.isdropdown = true;
			$scope.filtervalues = [];
			$scope.isFilterSelected = false;
			$scope.priceentered = null;
			$scope.updateView = function(filter,value,operator){
					var item;
					
					switch(filter){
							case 'Item':
								$scope.items = [];
								for(var i = 0; i < temp.length; i++){
										item = temp[i];
										if(item.item === value){
										
											$scope.items.push(item);
										}
								}
								break;
							case 'Vendor':
								$scope.items = [];
								for(var i = 0; i < temp.length; i++){
										item = temp[i];
										if(item.vendor === value)
											$scope.items.push(item);
								}
								break;
							case 'Price':
								$scope.items = [];
								for(var i = 0 ;i < temp.length ; i++){
										item = temp[i];
										if(operator === 'greaterthan'){
											if(item.price > value)
												$scope.items.push(item);
										}
										else if(operator === 'lessthan'){
											if(item.price < value)
												$scope.items.push(item);
										}
										else{
											if(item.price == value)
												$scope.items.push(item);
										}
								}
								break;
					}
					

			};

			$scope.updatefilervalues = function(value){
					var item,key;
					if(value === 'Item'){
							$scope.isdropdown = true;
							$scope.isFilterSelected = true;
							$scope.filtervalues = [];
							for(key in hashMap){
								$scope.filtervalues.push(key);	
							}
					}
					else if(value === 'Vendor'){
							$scope.isdropdown = true;
							$scope.isFilterSelected = true;
							$scope.filtervalues = [];
							for(key in uniqueVendors){
				
								$scope.filtervalues.push(key);
								
							}
					}
					else if(value === 'Price'){
							$scope.isdropdown = false;
							$scope.isFilterSelected = true;

					}
					else{
							$scope.isFilterSelected = false;
							$scope.isdropdown = true;
					}
			};

			$scope.$watch('selectedFilter',function(newValue,oldValue){
					if(newValue!=oldValue){
							
							$scope.updatefilervalues(newValue);
					}
			});

			$scope.$watch('isFilterSelected',function(newValue,oldValue){
						if(newValue === false){
								$scope.items = temp;
								$scope.selectedFilter = '';
						}
			});


	}]);


	MainApp.config(function($mdThemingProvider){
			$mdThemingProvider.theme('default')
			.primaryPalette('green')
			.accentPalette('red');

	});

})(socket,angular);