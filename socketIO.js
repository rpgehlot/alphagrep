/* Main socketio Handling on nodejs web server */
var fs = require('fs');
module.exports = function(socketIO,IOClient){

	// listening for the websocket connection from clients.
	socketIO.on('connection',function(socket){
			console.log('Got a connection.');
			
			/* 
				listening for the `getPreviousData` event. Its listener serves the data that has been saved from the time nodeserver has begun listening to socket server till the time this client has connected to nodejs server.i.e. intermediate data which is stored in a file `/tmp/data.json` and it is read using a readstream and eventually delievered to the client by emitting event named as `previousData` which client browser is listening to.
			*/
 			socket.on('getPreviousData',function(){
					
					
					var 
						// variable to hold the content of the file in string format.
						res='',

						// reading using a readstream from file.
						readfilestream = fs.createReadStream('/tmp/data.json');

					// listening for `data` event giving us chunks.	
					readfilestream.on('data',function(data){					
							res += data;
					});

					// listening for the end of the stream.
					readfilestream.on('end',function(){
							var arr = "[";
							res = res.substring(0,res.length-1);
							arr +=res;
							arr += "]";
							console.log(arr);
							// emitting the `previousData` event along with the previousData.
							socket.emit('previousData',arr);
					});

			});

			// notifying if the user has disconnected from the server or reload his/her page.
			socket.on('disconnect', function () {
				console.log('user disconnected from server');
			});
	});



	// listener for connecting to the socket server.
	IOClient.on('connect' , function(){
			console.log('nodejs server connected to the remote socketio server');
	});


	// listening for the `data` event from the socketio server.
	// in case event name is different on socket server.please change the name here.

	IOClient.on('data',function(data){
			var str = '';
			for ( var i = 0 ; i < data.length ; i++){
				str += JSON.stringify(data[i]);
				if(i < data.length - 1)
					str+=',\n';
				else str+=',';
			}

			// putting the data into the file for new clients.
			fs.appendFile('/tmp/data.json',str,function(err){
					if(err)
						console.error('error happened');
					else console.log('successfully written to the file');
			});
			
			// emitting the event to all the client browsers connected to the nodejs server and giving them the real time data.
			socketIO.sockets.emit('_data_',JSON.stringify(data));
			// rs.pipe(writableStream);
	});

	

};



















