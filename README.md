To Setup the application follow the following steps.

Use git clone to clone the repository.
	- git clone https://rpgehlot@bitbucket.org/rpgehlot/alphagrep.git
	- setup the application dependencies using 'npm install'.


1) first setup the test socket server.
	-To check with the built socket server, navigate to 'socketserversetup' folder and 
	run the command - 'node cron.js' or 'nodemon cron.js'.


	NOTE : CHANGE THE SOCKET_SERVER_URL VARIABLE IN THE APP.JS FILE IN ROOT directory to connect to your own socket server.


2) Now start the nodejs web server from the root directory using 
	the command 'node app.js'.

3) Once this starts , you can access the application in the browser at
		http://localhost:(whateverportyouhave set in the app) or by default it opens in 
		http://localhost:8080;

